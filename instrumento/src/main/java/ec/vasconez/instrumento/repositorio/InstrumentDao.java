package ec.vasconez.instrumento.repositorio;

import ec.vasconez.instrumento.entidad.Instrument;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author blankiss on 24/12/17.
 */
public interface InstrumentDao extends JpaRepository<Instrument, String> {


}
