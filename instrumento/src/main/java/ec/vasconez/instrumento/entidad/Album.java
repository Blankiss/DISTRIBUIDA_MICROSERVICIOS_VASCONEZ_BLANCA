package ec.vasconez.instrumento.entidad;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author blankiss on 24/12/17.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "album")
public class Album implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SINGER_ID")
    private Singer singer;

    @Column(name = "TITLE", length = 60)
    private String title;

    @Temporal(TemporalType.DATE)
    @Column(name = "RELEASE_DATE", length = 7)
    private Date releaseDate;

    @Version
    private int version;

    @Override
    public String toString() {
        return "Album - Id: " + id + ", Title: " +
                title + ", Release Date: " + releaseDate;
    }
}
