package ec.vasconez.instrumento.rest;


import ec.vasconez.instrumento.entidad.Instrument;
import ec.vasconez.instrumento.repositorio.InstrumentDao;
import ec.vasconez.instrumento.serv.DBInitializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author blankiss on 24/12/17.
 */
@RestController
public class InstrumentService {

    private Logger logger =
            LoggerFactory.getLogger(DBInitializer.class);

    private final InstrumentDao dao;
    private final RabbitTemplate rabbitTemplate;

    @Value("${app.queueName}")
    String queueName;

    public InstrumentService(InstrumentDao dao, RabbitTemplate rabbitTemplate) {
        this.dao = dao;
        this.rabbitTemplate = rabbitTemplate;
    }

    @PostMapping("/instrumentos")
    public void saveInstrument(@RequestBody Instrument instrument) {
        logger.info("saveInstrument : " + instrument.toString());
        Instrument instrumentOld = dao.findOne(instrument.getInstrumentId());

        if (instrumentOld == null) {
            rabbitTemplate.convertAndSend(queueName, instrument.toString());
        } else {
            instrument.setSingers(instrumentOld.getSingers());
        }

        dao.save(instrument);
    }

    @DeleteMapping("/instrumentos/{id}")
    public void deleteInstrument(@PathVariable String id) {
        dao.delete(id);
    }

    @GetMapping("/instrumentos")
    public List<Instrument> findAll() {
        return dao.findAll();
    }
}
