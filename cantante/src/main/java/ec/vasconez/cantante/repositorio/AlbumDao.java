package ec.vasconez.cantante.repositorio;

import ec.vasconez.cantante.entidad.Album;
import ec.vasconez.cantante.entidad.Singer;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author blankiss on 24/12/17.
 */
public interface AlbumDao extends JpaRepository<Album, Long> {

}
