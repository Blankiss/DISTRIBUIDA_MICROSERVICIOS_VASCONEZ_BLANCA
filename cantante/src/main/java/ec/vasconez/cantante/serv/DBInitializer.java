package ec.vasconez.cantante.serv;

import ec.vasconez.cantante.entidad.Album;
import ec.vasconez.cantante.entidad.Instrument;
import ec.vasconez.cantante.entidad.Singer;
import ec.vasconez.cantante.repositorio.SingerDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * @author blankiss on 24/12/17.
 */
@Service
public class DBInitializer {

	private Logger logger = LoggerFactory.getLogger(DBInitializer.class);

	private final SingerDao singerDao;

	public DBInitializer(SingerDao singerDao) {

		this.singerDao = singerDao;
	}

	@PostConstruct
	public void initDB() {
		logger.info("Starting database initialization...");
		Instrument guitar = new Instrument();
		guitar.setInstrumentId("Guitar");

		Instrument piano = new Instrument();
		piano.setInstrumentId("Piano");

		Singer singer = new Singer();
		singer.setId(158L);
		singer.setFirstName("John");
		singer.setLastName("Mayer");
		singer.setBirthDate(new Date((new GregorianCalendar(1977, 9, 16)).getTime().getTime()));
		singer.addInstrument(guitar);
		singer.addInstrument(piano);
		Album album1 = new Album();
		album1.setTitle("The Search For Everything");
		album1.setReleaseDate(new java.sql.Date((new GregorianCalendar(2017, 0, 20)).getTime().getTime()));
		singer.addAbum(album1);
		Album album2 = new Album();
		album2.setTitle("Battle Studies");
		album2.setReleaseDate(new java.sql.Date((new GregorianCalendar(2009, 10, 17)).getTime().getTime()));
		singer.addAbum(album2);
		singer.setEmail("blanki_vasconez@hotmail.com");
		singerDao.save(singer);

		logger.info("Database initialization finished.");
	}
}
