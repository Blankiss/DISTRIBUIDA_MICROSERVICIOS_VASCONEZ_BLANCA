package ec.vasconez.cantante.serv;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class EmailService {

    private final JavaMailSender emailSender;

    public EmailService(JavaMailSender emailSender) {
        this.emailSender = emailSender;
    }

    @Async
    public void sendEmail(List<String> emails, String msg) {
    	System.out.println(emails);
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(emails.toArray(new String[emails.size()]));
        message.setSubject("Nuevo instrumento");
        message.setText("Nuevo instrumento agregado: " + msg);
        emailSender.send(message);
    }
}
