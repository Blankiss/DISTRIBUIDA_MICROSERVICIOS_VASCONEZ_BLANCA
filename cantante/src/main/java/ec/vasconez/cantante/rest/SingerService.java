package ec.vasconez.cantante.rest;


import ec.vasconez.cantante.entidad.Singer;
import ec.vasconez.cantante.repositorio.AlbumDao;
import ec.vasconez.cantante.repositorio.SingerDao;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author blankiss on 24/12/17.
 */
@RestController
public class SingerService {

    private final SingerDao dao;
    private final AlbumDao albumDao;

    public SingerService(SingerDao dao, AlbumDao albumDao) {
        this.dao = dao;
        this.albumDao = albumDao;
    }


    @PostMapping("/cantante")
    public void save(@RequestBody Singer singer) {
        dao.save(singer);
    }

    @DeleteMapping("/cantante/{id}")
    public void delete(@PathVariable Long id) {
        dao.delete(id);
    }

    @GetMapping("/cantante")
    public List<Singer> findAll() {
        return dao.findAll();
    }
}
