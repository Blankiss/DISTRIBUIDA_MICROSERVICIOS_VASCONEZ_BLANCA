package ec.vasconez.cantante.rest;


import ec.vasconez.cantante.entidad.Album;
import ec.vasconez.cantante.repositorio.AlbumDao;
import ec.vasconez.cantante.repositorio.SingerDao;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author blankiss on 24/12/17.
 */
@RestController
public class AlbumService {

    private final SingerDao dao;
    private final AlbumDao albumDao;

    public AlbumService(SingerDao dao, AlbumDao albumDao) {
        this.dao = dao;
        this.albumDao = albumDao;
    }


    @PostMapping("/album")
    public void save(@RequestBody Album album) {
        albumDao.save(album);
    }

    @DeleteMapping("/album/{id}")
    public void delete(@PathVariable Long id) {
        albumDao.delete(id);
    }

    @GetMapping("/album")
    public List<Album> findAll() {
        return albumDao.findAll();
    }
}
