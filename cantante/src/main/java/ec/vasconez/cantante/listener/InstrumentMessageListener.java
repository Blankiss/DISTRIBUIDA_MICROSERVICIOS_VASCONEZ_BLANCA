package ec.vasconez.cantante.listener;

import ec.vasconez.cantante.entidad.Singer;
import ec.vasconez.cantante.repositorio.SingerDao;
import ec.vasconez.cantante.serv.EmailService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author dacopanCM on 25/02/18.
 */
@Component
public class InstrumentMessageListener {

    private static final Logger log = LogManager.getLogger(InstrumentMessageListener.class);

    private final SingerDao singerDao;
    private final EmailService emailService;

    public InstrumentMessageListener(SingerDao singerDao, EmailService emailService) {
        this.singerDao = singerDao;
        this.emailService = emailService;
    }

    public void receiveMessage(String instrument) {
        log.info("Received <" + instrument + ">");

        List<String> emails = singerDao.findAll().stream().map(Singer::getEmail).collect(Collectors.toList());
        if (emails.size() > 0) {
            emailService.sendEmail(emails, instrument);
        }

    }
}
