new Vue({
    //delimiters: ['${', '}$'],
    el: '#app',
    data: {
        list: [],
        currentItem: {},
        instrumentos: []
    },
    http: {
        root: 'http://localhost:8080/api/',
    },
    methods: {
        getData: function () {
            setTimeout(() => {
                this.$http.get('cantante/cantante/').then(function (data, status, request) {
                    if (data.status == 200) {
                        this.list = data.body;
                    }
                })

            }, 500);
        },
        eliminar: function (id) {
            this.$http.delete('cantante/cantante/' + id);
            this.getData();
        },
        guardar: function (item) {
            $('#modal').modal('hide');
            this.$http.post('cantante/cantante/', item);
            this.getData();
        },
        editar: function (item) {
            this.currentItem = item;
            this.$http.get('instrumento/instrumentos/').then(function (data, status, request) {
                if (data.status == 200) {
                    this.instrumentos = data.body;
                }
            })
            $('#modal').modal('show');
        },
        nuevo: function () {
            this.currentItem = {};
            this.$http.get('instrumento/instrumentos/').then(function (data, status, request) {
                if (data.status == 200) {
                    this.instrumentos = data.body;
                }
            });
            $('#modal').modal('show');
        },
        nuevoAlbum: function () {
            this.currentItem.albums.push({});
        },
        eliminarAlbum: function (alb) {
            this.currentItem.albums.remove(alb);
        }
    },
    mounted: function () {
        this.getData();
    }
})
