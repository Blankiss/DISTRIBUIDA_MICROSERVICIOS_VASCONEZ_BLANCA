new Vue({
    //delimiters: ['${', '}$'],
    el: '#app',
    data: {
        list: [],
        currentItem: {}
    },
    http: {
        root: 'http://localhost:8080/api/instrumento',
    },
    methods: {
        getData: function () {
            setTimeout(() => {
                this.$http.get('instrumentos/').then(function (data, status, request) {
                    if (data.status == 200) {
                        this.list = data.body;
                    }
                })
            }, 500);
        },
        eliminar: function (id) {
            this.$http.delete('instrumentos/' + id);
            this.getData();
        },
        guardar: function (item) {
            $('#modal').modal('hide');
            this.$http.post('instrumentos/', item);
            this.getData();
        },
        editar: function (item) {
            this.currentItem = item;
            $('#modal').modal('show');
        },
        nuevo: function (item) {
            this.currentItem = {};
            $('#modal').modal('show');
        }
    },
    mounted: function () {
        this.getData();
    }
})
