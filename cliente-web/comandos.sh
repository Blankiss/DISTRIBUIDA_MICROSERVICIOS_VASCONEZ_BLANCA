# stop remove all
sudo docker stop $(sudo docker ps -a -q)
sudo docker rm $(sudo docker ps -a -q)

# start network
sudo docker network create blankiss-net

# start database
sudo docker stop musicadb
sudo docker rm musicadb
sudo docker run --rm -d --network blankiss-net --name musicadb -e POSTGRES_USER=musicadb -e POSTGRES_PASSWORD=musicadb postgres

# start rabitmq
sudo docker stop my-rabbit
sudo docker rm my-rabbit
sudo docker run --rm -d --network blankiss-net --name my-rabbit --hostname my-rabbit -e RABBITMQ_DEFAULT_USER=guest -e RABBITMQ_DEFAULT_PASS=guest rabbitmq:3-management



# config server
cd config-server
./gradlew bootRepackage
sudo docker build -t blankiss/config-server .
sudo docker stop config-server || true && sudo docker rm config-server
sudo docker run -d --network blankiss-net --name config-server blankiss/config-server
cd ..



cd eureka
./gradlew bootRepackage
sudo docker build -t blankiss/eureka .
sudo docker stop eureka || true && sudo docker rm eureka
sudo docker run -d --network blankiss-net --name eureka -p 8761:8761 blankiss/eureka
cd ..



cd zuul
./gradlew bootRepackage
sudo docker build -t blankiss/zuul .
sudo docker stop zuul || true && sudo docker rm zuul
sudo docker run -d --network blankiss-net --name zuul -p 8080:8080 blankiss/zuul
cd ..



cd cantante
./gradlew bootRepackage
sudo docker build -t blankiss/cantante .
sudo docker run --rm --network blankiss-net -p 8082:8082 -d blankiss/cantante
sudo docker run --rm --network blankiss-net -p 8082:8082 blankiss/cantante
cd ..



cd instrumento
./gradlew bootRepackage
sudo docker build -t blankiss/instrumento .
sudo docker run --rm --network blankiss-net -p 8081:8081 -d blankiss/instrumento
sudo docker run --rm --network blankiss-net -p 8081:8081 blankiss/instrumento
cd ..




# web
cd cliente-web
sudo docker build -t blankiss/web .
sudo docker run --rm --network blankiss-net -p 80:80 -d blankiss/web


sudo docker ps
sudo docker stop
sudo docker start
sudo docker exec -it 787bfbb361c9 /bin/sh